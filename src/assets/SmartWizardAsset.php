<?php

namespace codesk\architectui\assets;

use yii\web\AssetBundle;

class SmartWizardAsset extends AssetBundle {

    public $sourcePath = '@bower/smartwizard/dist';
    public $js = [
        'js/jquery.smartWizard.min.js',
    ];
    public $css = [
        'css/smart_wizard_all.min.css',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

}
