<?php

namespace codesk\architectui\assets;

use yii\web\AssetBundle;

class PerfectScrollbarAsset extends AssetBundle {

    public $sourcePath = '@bower/utatti-perfect-scrollbar/dist';
    public $js = [
        'perfect-scrollbar.min.js',
    ];

}
