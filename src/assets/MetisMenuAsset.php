<?php

namespace codesk\architectui\assets;

use yii\web\AssetBundle;

class MetisMenuAsset extends AssetBundle {

    public $sourcePath = '@bower/metismenu/dist';
    public $js = [
        'metisMenu.min.js',
    ];

}
