<?php

namespace codesk\architectui\assets;

use yii\web\AssetBundle;

class PopperJsAsset extends AssetBundle {

    public $sourcePath = '@bower/popper.js/dist';
    public $js = [
        'popper.min.js',
    ];

}
