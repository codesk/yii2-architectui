<?php

namespace codesk\architectui\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle {

    public $js = [
        'scripts/app.js',
        'scripts/demo.js',
        'scripts/perfect-scrollbar.min.js',
    ];
    public $css = [
        'css/linear-font.css',
        'css/main.css',
        'css/custom.css',
    ];

    public function init() {
        $this->sourcePath = __DIR__ . '/../web';
        $this->publishOptions['forceCopy'] = defined('YII_DEBUG') && YII_DEBUG;
        parent::init();
    }

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'codesk\architectui\assets\MetisMenuAsset',
        'codesk\architectui\assets\SmartWizardAsset'
    ];

}
