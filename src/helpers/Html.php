<?php

namespace codesk\architectui\helpers;

use yii\bootstrap4\Html as YiiHtml;

class Html extends YiiHtml {

    public static function icon($name, $options = []) {
        Html::addCssClass($options, [
            'fa',
            'fa-' . $name,
        ]);
        return self::tag('i', '', $options);
    }

    public static function icon7s($name, $options = []) {
        self::addCssClass($options, 'metismenu-icon pe-7s-' . $name);
        return self::tag('i', '', $options);
    }

}
