<?php

namespace codesk\architectui\widgets;

use codesk\architectui\helpers\Html;
use yii\base\Widget;

class HeaderUser extends Widget {

    public $title;
    public $description;
    public $items = [];

    public function run() {
        $ret = [];
        $ret[] = parent::run();

        $ret[] = Html::beginTag('div', ['class' => 'header-btn-lg pr-0']);
        $ret[] = Html::beginTag('div', ['class' => 'widget-content p-0']);
        $ret[] = Html::beginTag('div', ['class' => 'widget-content-wrapper']);

        # User Menu
        $ret[] = Html::beginTag('div', ['class' => 'widget-content-left']);
        $ret[] = Html::beginTag('div', ['class' => 'btn-group']);
        $ret[] = Html::beginTag('a', ['class' => 'p-0 btn', 'data-toggle' => 'dropdown', 'aria-haspopup' => 'true', 'aria-expanded' => 'false']);
        $ret[] = Html::img('@web/images/user.png', ['class' => 'rounded-circle', 'width' => 42]);
        if (count($this->items)) {
            $ret[] = Html::tag('i', '', ['class' => 'fa fa-angle-down ml-2 opacity-8']);
        }
        $ret[] = Html::endTag('a');
        $ret[] = Html::endTag('div');
        $ret[] = Html::endTag('div');

        # User Info
        $ret[] = Html::beginTag('div', ['class' => 'widget-content-left ml-3 header-user-info']);
        $ret[] = Html::tag('div', $this->title, ['class' => 'widget-heading']);
        $ret[] = Html::tag('div', $this->description, ['class' => 'widget-subheading']);
        $ret[] = Html::endTag('div');

        $ret[] = Html::endTag('div');
        $ret[] = Html::endTag('div');
        $ret[] = Html::endTag('div');
        return implode("\n", $ret);
    }

}
