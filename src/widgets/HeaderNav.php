<?php

namespace codesk\architectui\widgets;

use codesk\architectui\helpers\Html;
use yii\bootstrap4\Nav;

class HeaderNav extends Nav {

    public $encodeLabels = false;

    public function init() {
        parent::init();
        Html::addCssClass($this->options, [
            'header-menu',
        ]);
    }

}
