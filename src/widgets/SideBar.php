<?php

namespace codesk\architectui\widgets;

use codesk\architectui\helpers\Html;
use yii\base\Widget;

class SideBar extends Widget {

    public $items = [];

    public function run() {
        $ret = [];
        $ret[] = parent::run();

        $ret[] = Html::beginTag('div', ['class' => ['app-sidebar', 'sidebar-shadow']]);

        # Header Logo
        $ret[] = Html::beginTag('div', ['class' => 'app-header__logo']);
        $ret[] = Html::tag('div', '', ['class' => 'logo-src']);
        $ret[] = Html::beginTag('div', ['class' => 'header__pane ml-auto']);
        $ret[] = Html::beginTag('div');
        $ret[] = Html::button(Html::tag('span', Html::tag('span', '', ['class' => 'hamburger-inner']), ['class' => 'hamburger-box']), ['class' => 'hamburger close-sidebar-btn hamburger--elastic', 'data-class' => 'closed-sidebar']);
        $ret[] = Html::endTag('div');
        $ret[] = Html::endTag('div');
        $ret[] = Html::endTag('div');

        # Mobile Menu
        $ret[] = Html::beginTag('div', ['class' => 'app-header__mobile-menu']);
        $ret[] = Html::beginTag('div');
        $ret[] = Html::button(Html::tag('span', Html::tag('span', '', ['class' => 'hamburger-inner']), ['class' => 'hamburger-box']), ['class' => 'hamburger hamburger--elastic mobile-toggle-nav']);
        $ret[] = Html::endTag('div');
        $ret[] = Html::endTag('div');

        # Icon Menu
        $ret[] = Html::beginTag('div', ['class' => 'app-header__menu']);
        $ret[] = Html::beginTag('span');
        $ret[] = Html::button(Html::tag('span', Html::tag('i', '', ['class' => 'fa fa-ellipsis-v fa-w-6']), ['class' => 'btn-icon-wrapper']), ['class' => 'btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav']);
        $ret[] = Html::endTag('span');
        $ret[] = Html::endTag('div');

        # Side Menu
        $ret[] = Html::beginTag('div', ['class' => 'scrollbar-sidebar']);
        $ret[] = Html::beginTag('div', ['class' => 'app-sidebar__inner']);
        $ret[] = SideBarNav::widget([
                    'items' => $this->items,
        ]);
        $ret[] = Html::endTag('div');
        $ret[] = Html::endTag('div');

        $ret[] = Html::endTag('div');
        return implode("\n", $ret);
    }

}
