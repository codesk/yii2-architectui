<?php

namespace codesk\architectui\widgets;

use codesk\architectui\helpers\Html;
use yii\base\Widget;
use yii\bootstrap4\Nav;

class PageTitle extends Widget {

    public $title;
    public $subtitle;
    public $actions;
    public $icon;
    public $iconType = '7s';
    public $iconOptions = [];
    public $iconWrapperOptions = [];
    public $wrapperOptions = [];
    public $pageOptions = [];

    public function run(): string {

        Html::addCssClass($this->pageOptions, 'app-page-title');
        Html::addCssClass($this->wrapperOptions, 'page-title-wrapper');

        $html = [];
        $html[] = Html::beginTag('div', $this->pageOptions);
        $html[] = Html::beginTag('div', $this->wrapperOptions);
        $html[] = $this->renderHeading();
        $html[] = $this->renderActions();
        $html[] = Html::endTag('div');
        $html[] = Html::endTag('div');
        return implode('', $html);
    }

    public function renderHeading() {
        $html = [];
        $html[] = Html::beginTag('div', ['class' => 'page-title-heading']);
        if (isset($this->icon)) {
            Html::addCssClass($this->iconWrapperOptions, 'page-title-icon');
            $html[] = Html::beginTag('div', $this->iconWrapperOptions);
            if ($this->iconType === '7s') {
                $html[] = Html::icon7s($this->icon, $this->iconOptions);
            } else {
                $html[] = Html::img($this->icon, $this->iconOptions);
            }
            $html[] = Html::endTag('div');
        }
        $html[] = Html::beginTag('div');
        $html[] = $this->title;
        $html[] = Html::tag('div', $this->subtitle, ['class' => 'page-title-subheading']);
        $html[] = Html::endTag('div');

        $html[] = Html::endTag('div');
        return implode('', $html);
    }

    public function renderActions() {
        $html = [];
        $html[] = Html::beginTag('div', ['class' => 'page-title-actions']);
        $html[] = Nav::widget($this->actions);
        $html[] = Html::endTag('div');
        return implode('', $html);
    }

}

?>