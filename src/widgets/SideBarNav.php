<?php

namespace codesk\architectui\widgets;

use codesk\architectui\helpers\Html;
use Exception;
use yii\base\InvalidConfigException;
use yii\bootstrap4\Nav;
use yii\helpers\ArrayHelper;

class SideBarNav extends Nav {

    public $dropdownClass = 'codesk\architectui\widgets\Dropdown';

    public function init() {
        parent::init();
        Html::removeCssClass($this->options, ['widget' => 'nav']);
        Html::addCssClass($this->options, [
            'vertical-nav-menu',
            'metismenu',
        ]);
    }

    /**
     * Renders a widget's item.
     * @param string|array $item the item to render.
     * @return string the rendering result.
     * @throws InvalidConfigException
     * @throws Exception
     */
    public function renderItem($item) {
        if (is_string($item)) {
            return Html::tag('li', $item, ['class' => 'app-sidebar__heading']);
        }
        if (!isset($item['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        if (isset($item['visible']) && !$item['visible']) {
            return;
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
        $options = ArrayHelper::getValue($item, 'options', []);
        $items = ArrayHelper::getValue($item, 'items');
        $url = ArrayHelper::getValue($item, 'url', '#');
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);
        $disabled = ArrayHelper::getValue($item, 'disabled', false);
        $active = $this->isItemActive($item);

        $label = isset($item['icon']) ? Html::tag('i', '', ['class' => 'metismenu-icon pe-7s-' . $item['icon']]) . $label : $label;



        if (empty($items)) {
            $items = '';
        } else {
            $linkOptions['aria-expanded'] = 'false';
            if (is_array($items)) {
                if ($active) {
                    $label .= '<i class="metismenu-state-icon pe-7s-angle-up caret-left"></i>';
                } else {
                    $label .= '<i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>';
                }
                $items = $this->isChildActive($items, $active);
                $items = $this->renderDropdown($items, $item);
            }
        }

        //Html::addCssClass($options, 'nav-item');
        //Html::addCssClass($linkOptions, 'nav-link');

        if ($disabled) {
            ArrayHelper::setValue($linkOptions, 'tabindex', '-1');
            ArrayHelper::setValue($linkOptions, 'aria-disabled', 'true');
            Html::addCssClass($linkOptions, 'mm-disabled');
        } elseif ($this->activateItems && $active) {
            Html::addCssClass($linkOptions, 'mm-active');
        }

        return Html::tag('li', Html::a($label, $url, $linkOptions) . $items, $options);
    }

    /**
     * Renders the given items as a dropdown.
     * This method is called to create sub-menus.
     * @param array $items the given items. Please refer to [[Dropdown::items]] for the array structure.
     * @param array $parentItem the parent item information. Please refer to [[items]] for the structure of this array.
     * @return string the rendering result.
     * @throws \Exception
     */
    protected function renderDropdown($items, $parentItem) {
        /** @var Widget $dropdownClass */
        $dropdownClass = $this->dropdownClass;
        return $dropdownClass::widget([
                    'options' => ArrayHelper::getValue($parentItem, 'dropdownOptions', []),
                    'items' => $items,
                    'encodeLabels' => $this->encodeLabels,
                    'clientOptions' => false,
                    'view' => $this->getView(),
        ]);
    }

}
