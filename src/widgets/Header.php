<?php

namespace codesk\architectui\widgets;

use codesk\architectui\helpers\Html;
use yii\base\Widget;

class Header extends Widget {

    public $headerOptions = [];

    public function init() {
        parent::init();
        ob_start();
    }

    public function run() {
        $ret = [];
        $ret[] = parent::run();


        # Header START
        Html::addCssClass($this->headerOptions, [
            'app-header',
            'header-shadow',
        ]);
        $ret[] = Html::beginTag('div', $this->headerOptions);

        # Header Logo
        $ret[] = Html::beginTag('div', ['class' => 'app-header__logo']);
        $ret[] = Html::tag('div', '', ['class' => 'logo-src']);
        $ret[] = Html::beginTag('div', ['class' => 'header__pane ml-auto']);
        $ret[] = Html::beginTag('div');
        $ret[] = Html::button(Html::tag('span', Html::tag('span', '', ['class' => 'hamburger-inner']), ['class' => 'hamburger-box']), ['class' => 'hamburger close-sidebar-btn hamburger--elastic', 'data-class' => 'closed-sidebar']);
        $ret[] = Html::endTag('div');
        $ret[] = Html::endTag('div');
        $ret[] = Html::endTag('div');

        # Header Mobile Menu
        $ret[] = Html::beginTag('div', ['class' => 'app-header__mobile-menu']);
        $ret[] = Html::beginTag('div');
        $ret[] = Html::button(Html::tag('span', Html::tag('span', '', ['class' => 'hamburger-inner']), ['class' => 'hamburger-box']), [
                    'class' => [
                        'hamburger',
                        'hamburger--elastic',
                        'mobile-toggle-nav',
                    ],
        ]);
        $ret[] = Html::endTag('div');
        $ret[] = Html::endTag('div');

        # Header Menu
        $ret[] = Html::beginTag('div', ['class' => 'app-header__menu']);
        $ret[] = Html::beginTag('span');
        $ret[] = Html::button(Html::tag('span', Html::tag('i', '', ['class' => 'fa fa-ellipsis-v fa-w-6']), ['class' => 'btn-icon-wrapper']), [
                    'class' => [
                        'btn',
                        'btn-sm',
                        'btn-icon',
                        'btn-icon-only',
                        'btn-primary',
                        'mobile-toggle-header-nav',
                    ],
        ]);
        $ret[] = Html::endTag('span');
        $ret[] = Html::endTag('div');

        # Header Content
        $ret[] = Html::beginTag('div', ['class' => 'app-header__content']);
        $ret[] = ob_get_clean();
        $ret[] = Html::endTag('div');

        # Header END
        $ret[] = Html::endTag('div');
        return implode("\n", $ret);
    }

}
