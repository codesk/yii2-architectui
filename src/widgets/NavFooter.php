<?php

namespace codesk\architectui\widgets;

use codesk\architectui\helpers\Html;
use yii\base\Widget;
use yii\bootstrap4\Nav;


class NavFooter extends Widget {

    public $leftItems = [];
    public $leftOptions = [];
    public $rightItems = [];
    public $rightOptions = [];

    public function run() {
        $ret = [];
        $ret[] = parent::run();
        $ret[] = Html::beginTag('div', ['class' => 'app-wrapper-footer']);
        $ret[] = Html::beginTag('div', ['class' => 'app-footer']);
        $ret[] = Html::beginTag('div', ['class' => 'app-footer__inner']);



        if (is_countable($this->leftItems)) {
            $this->leftOptions['items'] = $this->leftItems;
            $ret[] = Html::beginTag('div', ['class' => 'app-footer-left']);
            $ret[] = Nav::widget($this->leftOptions);
            $ret[] = Html::endTag('div');
        }

        if (is_countable($this->rightItems)) {
            $this->rightOptions['items'] = $this->rightItems;
            $ret[] = Html::beginTag('div', ['class' => 'app-footer-right']);
            $ret[] = Nav::widget($this->rightOptions);
            $ret[] = Html::endTag('div');
        }

        $ret[] = Html::endTag('div');
        $ret[] = Html::endTag('div');
        $ret[] = Html::endTag('div');
        return implode("\n", $ret);
    }

}
